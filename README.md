# Learning Chef

This repository contain some files that helps to learn chef in a docker environment. The docker-compose.yml file produces an environment with one container as a workstation and three containers that represent target environments. We'll configure one target as a LoadBalancer and two targets as a Web Application.

# Pre-requistes

* docker
* docker-compose

## How to use

First of all, get the images from Docker Hub to your local environment:

```bash
docker-compose pull
```

Build the containers:

```bash
docker-compose build
```

Start the containers in background:

```bash
docker-compose up -d
```

Access the workstation container:

```bash
docker exec -it workstation bash
```

Create resources on target machines with chef-run passing some standalone resources and check the results

```bash
chef-run web1 file hello.txt
ssh web1 ls -ltr /hello.txt
chef-run web1 file hello.txt content="Hello World!"
ssh web1 cat /hello.txt
chef-run web1 file hello.txt action=delete
ssh web1 ls -ltr /hello.txt
```

Execute recipes on target machines with chef-run and check the results

```bash
cd /opt/recipes
chef-run web1 recipe.rb
ssh web1 cat /tmp/hello.txt
chef-run web1 example.rb
ssh web1 cat /foo/bar
```

Execute cookbooks on target machines with chef-run and check the results

```bash
cd /opt/cookbooks
chef-run web[1:2] webserver
curl web1
curl web2
chef-run lb loadbalancer
curl lb (Execute more than one time to reach different containers)
```

# Clean your environment

```bash
docker-compose down --rmi all
```