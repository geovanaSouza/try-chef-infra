directory '/foo'

file '/foo/bar' do
    content 'Hello World!'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end
