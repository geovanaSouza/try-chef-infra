name 'loadbalancer'
maintainer 'Geovana Possenti'
description 'Installs/Configures loadbalancer'
long_description 'Installs/Configure loadbalancer'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.
#
# issues_url 'https://gitlab.com/geovanaSouza/try-chef-infra/-/issues'

# The `source_url` points to the development repository for this cookbook.
#
# source_url 'https://gitlab.com/geovanaSouza/try-chef-infra'