#
# Cookbook:: loadbalancer
# Recipe:: default
#
apt_update
package 'haproxy'

directory '/etc/haproxy'

template '/etc/haproxy/haproxy.cfg' do
    source 'haproxy.cfg.erb'
    notifies :restart, 'service[haproxy]'
end

service 'haproxy' do
    action [:enable, :start]
end
